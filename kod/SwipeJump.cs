﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeJump : MonoBehaviour {

	private Vector3 startTouchPosition, endTouchPosition, o;
    public Vector3 jump;
    public float jumpForce = 2.0f;
    public bool isGrounded;
    private Rigidbody rb;
    void OnCollisionStay()
    {
        isGrounded = true;
    }

    // Use this for initialization
    private void Start () {
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 1.0f, 0.0f);
    }
	
	// Update is called once per frame
	private void Update () {
				
	}

	private void FixedUpdate()
	{
        if (Input.touchCount > 0 && isGrounded)
        {

            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
    }
}
   
	

	


