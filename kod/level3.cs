﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class level3 : MonoBehaviour
{
    public float speed;
    public Text CountText;
    public Text winText;
    private Rigidbody rb;
    private int count;
    public Vector3 jump;
    public float jumpForce = 2.0f;
    public bool isGrounded;
    void OnCollisionStay()
    {
        isGrounded = true;
    }

    private void PCcontrolling()
    {
        float movehorizontal = Input.GetAxis("Horizontal");
        float movevertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(movehorizontal, 0, movevertical);
        rb.AddForce(movement * speed);
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {

            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
    }

    private void AndroidControlling()
    {
        float moveHorizontal = Input.acceleration.x;
        float moveVertical = Input.acceleration.y;
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
    }
    IEnumerator ShowMessage(string message, float delay)
    {
        winText.text = message;
        winText.enabled = true;
        yield return new WaitForSeconds(delay);
        winText.enabled = false;
    }
    IEnumerator WaitFor(float delayy)
    {
        yield return new WaitForSeconds(delayy);
        SceneManager.LoadScene(4);

    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 2.0f, 0.0f);
        count = 0;
        setcounttext();
        StartCoroutine(ShowMessage("RAZINA 3", 2));
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
    private void FixedUpdate()
    {
        PCcontrolling();
        AndroidControlling();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            setcounttext();
        }
    }
    void setcounttext()
    {
        CountText.text = "Count: " + count.ToString();
        if (count >= 114)
        {
            StartCoroutine(ShowMessage("BRAVOO", 2));
            StartCoroutine(WaitFor(3));
        }
    }
}